// console.log("Hello Harold!")


/*
In the S16 folder, create an activity folder and an index.html and index.js file inside of it.
Link the index.js file to the index.html file.
Create a variable number that will store the value of the number provided by the user via the prompt.
Create a for loop that will be initialized with the number provided by the user, will stop when the value is less than or equal to 0 and will decrease by 5 every iteration.
Create a condition that if the current value is less than or equal to 50, stop the loop.
Create another condition that if the current value is divisible by 10, print a message that the number is being skipped and continue to the next iteration of the loop.
Create another condition that if the current value is divisible by 5, print the number.

Create a variable that will contain the string supercalifragilisticexpialidocious.
Create another variable that will store the consonants from the string.
Create another for Loop that will iterate through the individual letters of the string based on it’s length.
Create an if statement that will check if the letter of the string is equal to a vowel and continue to the next iteration of the loop if it is true.
Create an else statement that will add the letter to the second variable.
Create a git repository named S16.
Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
Add the link in Boodle.
*/

console.log("The number you provided is 100.");
for(let count = 100; count>0; count-=5){
	if(count<=50){
		console.log("The current value is 50. Terminating the loop.");
		break;
	}
	else if(count%10===0){
		console.log("The number is divisible by 10. Skipping the number:");
		continue;
	}
	console.log(count);
}

let word = "supercalifragilisticexpialidocious";
console.log(word)
let wordWithoutVowels = "";
for (let i=0; i<word.length; i++){
	if(word[i].toLowerCase() === "a" ||
		word[i].toLowerCase() === "e" ||
		word[i].toLowerCase() === "i" ||
		word[i].toLowerCase() === "o" ||
		word[i].toLowerCase() === "u"){
		continue;
		}
		else{
			wordWithoutVowels+=word[i];
		}
}
console.log(wordWithoutVowels);













































































































































































































































































































